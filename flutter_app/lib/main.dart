import 'package:flutter/material.dart';
import 'card.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fisrt Assignment',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: MyHomePage(
        title: 'HomeWork 1',
      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container (
          child: Column (
            children: <Widget>[
              MyCard(
                title: 'Name:',
                content: 'Benjamin Castres',
                color: 0xFFFF0000,
              ),
              MyCard(
                title: 'Graduation date:',
                content: 'Spring 2022',
                color: 0xFFFF5D00,
              ),
              MyCard(
                title: 'Quote:',
                content: 'Turn your face to the sun and the shadows fall behind you',
                color: 0xFFFFC100,
              ),
              MyCard(
                title: 'HomeWork purpose:',
                content: 'Hello World',
                color: 0xFFB2FF33,
              ),
            ],
          )
      ),
    );
  }
}
