import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyCard extends StatefulWidget {
  final title;
  final content;
  final color;

  MyCard({Key key, String title, String content, int color})
      : title = title, content = content, color = color, super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MyCardState();
  }
}

class _MyCardState extends State<MyCard> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Card (
      child: Container(
        decoration: BoxDecoration(
          color: Color(widget.color),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Theme
                  .of(context)
                  .brightness == Brightness.dark
                  ? Colors.black
                  : Colors.grey[400],
              blurRadius: 5.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 8.0),
              height: 0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text(widget.title),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 8.0),
              height: 0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Text(widget.content),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 8.0),
              height: 0,
            ),
          ],
        ),
      ),
    );
  }
}
